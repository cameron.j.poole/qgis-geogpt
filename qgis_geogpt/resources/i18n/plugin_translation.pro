FORMS = ../../gui/dlg_settings.ui

SOURCES= ../../plugin_main.py \
    ../../gui/dlg_settings.py \
    ../../toolbelt/log_handler.py \
    ../../toolbelt/preferences.py

TRANSLATIONS = qgis_geogpt_en.ts
