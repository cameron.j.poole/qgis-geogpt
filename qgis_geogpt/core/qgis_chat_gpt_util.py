"""Qgis Implementation of chat gpt. """
from PyQt5.QtCore import QVariant
from qgis.core import (
    QgsField,
    QgsFields,
    QgsMemoryProviderUtils,
    QgsProject,
    QgsVectorLayerUtils,
    edit,
)

from qgis_geogpt.core.chat_gpt import ChatGPTResponse, ChatGPTUtils
from qgis_geogpt.toolbelt import PlgLogger, PlgOptionsManager


class QgsChatGPTUtils(ChatGPTUtils):
    def __init__(self, api_key=None, api_model=None):
        self.plg_settings = PlgOptionsManager()
        super().__init__(
            api_key=api_key
            or self.plg_settings.get_value_from_key(
                "chat_gpt_api_key", default=None, exp_type=str
            ),
            api_model=api_model
            or self.plg_settings.get_value_from_key(
                "chat_gpt_api_model", default="gpt-3.5-turbo", exp_type=str
            ),
            log=PlgLogger(),
        )
        self.helper_layer_name = "chat_gpt_responses"
        self.layer = self.get_qgis_layer()

    def create_query_feet(self):
        """Create a feature for the query layer."""
        return QgsVectorLayerUtils.createFeature(self.layer)

    @staticmethod
    def set_feature_attributes(
        feature,
        response: ChatGPTResponse,
        overpass_query: str,
        improved_overpass_query: str,
    ):
        """Set the attributes of a feature."""
        feature.setAttributes(
            [
                response.id,
                response.object,
                response.created,
                str(response.choices),
                str(response.usage),
                response.model,
                overpass_query,
                improved_overpass_query,
            ]
        )

    def write_query_to_layer(
        self,
        response: ChatGPTResponse,
        overpass_query: str,
        improved_overpass_query: str,
    ):
        """Write the query to the layer."""
        feat = self.create_query_feet()
        self.set_feature_attributes(
            feature=feat,
            response=response,
            overpass_query=overpass_query,
            improved_overpass_query=improved_overpass_query,
        )
        with edit(self.layer):
            self.layer.addFeature(feat)

    def query_api(self, text: str) -> ChatGPTResponse:
        resp = super().query_api(text=text)
        return resp

    def log_message(self, messsage, level=1):
        self.log.log(message=messsage, log_level=level)

    @staticmethod
    def layer_fields():
        """Return the fields for the chat gpt layer."""
        fields = QgsFields()
        fields.append(QgsField("id", QVariant.String))
        fields.append(QgsField("object", QVariant.String))
        fields.append(QgsField("created", QVariant.Int))
        fields.append(QgsField("choices", QVariant.String))
        fields.append(QgsField("usage", QVariant.String))
        fields.append(QgsField("model", QVariant.String))
        fields.append(QgsField("overpass_query", QVariant.String))
        fields.append(QgsField("overpass_improved_query", QVariant.String))
        return fields

    def get_qgis_layer(self):
        """Chat GPT QGIS layer captures query information and gives us the ability to change the query and rerun etc"""
        # check if layer exists in current projects
        #  search for helper_layer_name in QgsProject().instance().mapLayers()
        if self.helper_layer_name in [
            vl.name() for vl in QgsProject().instance().mapLayers().values()
        ]:
            return (
                QgsProject().instance().mapLayersByShortName(self.helper_layer_name)[0]
            )
        layer = self.chat_gpt_qgis_layer()
        QgsProject().instance().addMapLayer(layer)
        return layer

    def chat_gpt_qgis_layer(self):
        """Chat GPT QGIS layer captures query information and gives us the ability to change the query and rerun etc."""
        return QgsMemoryProviderUtils.createMemoryLayer(
            name=self.helper_layer_name, fields=self.layer_fields()
        )
