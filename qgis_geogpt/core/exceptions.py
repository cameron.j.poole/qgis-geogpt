class FailedToFetchMessageError(Exception):
    pass


class FailedTogetOverpassDataError(Exception):
    pass


class NoQueryExtracted(Exception):
    pass
