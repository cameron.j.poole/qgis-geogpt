"""Overpass API encapsulation

See https://python-overpy.readthedocs.io/en/latest/

"""
import base64
import random
import re

from qgis.core import (
    QgsFeature,
    QgsField,
    QgsFields,
    QgsGeometry,
    QgsLineString,
    QgsPoint,
    QgsPolygon,
    QgsVectorLayer,
    edit,
)
from qgis.PyQt.QtCore import QVariant

from qgis_geogpt.core.chat_gpt import ChatGPTResponse
from qgis_geogpt.utils import overpy
from qgis_geogpt.utils.overpy.exception import DataIncomplete


class OverpassQueryUtils:
    @staticmethod
    def replace_comments(query):
        comments = {}
        # replace comments with placeholders
        # (we do not want to autorepair stuff which is commented out.)
        cs = re.findall(r"/\*[\s\S]*?\*/", query) or []  # multiline comments: /*...*/
        for csi in cs:
            placeholder = (
                "/*" + base64.b64encode(str(random.random()).encode()).decode() + "*/"
            )
            query = query.replace(csi, placeholder)
            comments[placeholder] = csi
        cs = re.findall(r"//[^\n]*", query) or []  # single line comments: //...
        for csi in cs:
            placeholder = (
                "/*" + base64.b64encode(str(random.random()).encode()).decode() + "*/"
            )
            query = query.replace(csi, placeholder)
            comments[placeholder] = csi
        return query, comments

    @staticmethod
    def fix_missing_recurse(q):
        outs = re.findall(r'(\n?[^\S\n]*(\.[^.;]+)?out[^:;"\]]*;)', q)
        for i, out_stmt in enumerate(outs):
            # given "\n    out body;" should get endline and spaces from the beginning
            out_stmt = out_stmt[0]
            ws = OverpassQueryUtils.get_whitespace(out_stmt)
            from_ = re.search(r"\.([^;.]+?)\s+out", out_stmt)
            if from_:
                add = "(.{};.{} >;)->.{};".format(
                    from_.group(1), from_.group(1), from_.group(1)
                )
            else:
                add = "(._;>;);"
            q = re.sub(out_stmt, f"\n{ws}{add}{ws}<autorepair>{i}</autorepair>", q)
        for i, out_stmt in enumerate(outs):
            out_stmt = out_stmt[0]
            q = re.sub(f"<autorepair>{i}</autorepair>", out_stmt, q)
        return q

    @staticmethod
    def get_whitespace(out_stmt: str):
        ws = re.match(r"^\n?(\s*)", out_stmt).group(1)
        return ws

    @staticmethod
    def fix_prints(q):
        # Work in Progress not really sure what it does or how to fix
        prints = re.findall(r'(\.([^;.]+?)\s+)?(out[^:;"\]]*;)', q)
        # for print_i in prints:
        #     print_m, out_set, out_statement = print_i
        #     if (
        #         out_statement.find(r"\s(body|skel|ids|tags)") >= 0
        #         or not out_statement.find(r"\s(meta)") >= 0
        #     ):
        #         new_out_statement = re.sub(
        #             r"\s(body|skel|ids|tags|meta)", "", out_statement
        #         )
        #         new_out_statement = re.sub(r"^out", "out meta", new_out_statement)
        #         new_print = new_print.replace(out_statement, new_out_statement)
        #         out_statement = new_out_statement
        #     if out_statement.find(r"\s(center|bb|geom)") >= 0:
        #         new_out_statement = re.sub(r"\s(center|bb|geom)", "", out_statement)
        #         new_print = new_print.replace(out_statement, new_out_statement)
        #         out_statement = new_out_statement
        #         if out_set:
        #             new_print = rf"(.{out_set};.{out_set} >;)->.{out_set}; {new_print}"
        #         else:
        #             new_print = rf"(._;>;); {new_print}"
        #     if new_print != print_m:
        #         q = q.replace(print, rf"{new_print}/*fixed by auto repair*/")
        return q

    @staticmethod
    def improve_query(query):
        """Improve the query. Try do some basic improvements,
        Duplicate nodes and ways if only one is present.
        Add out body and out skel qt if not present."""
        try:
            # convert [out:json] to [out:json][timeout:25] if not present
            query = OverpassQueryUtils.add_timeouts(query)
            query, comments = OverpassQueryUtils.replace_comments(query)
            # get leading white space from outs
            query = query.replace("out center;", f"out body;\nout skel qt;")
            query = OverpassQueryUtils.fix_missing_recurse(query)

            query = OverpassQueryUtils.add_node_or_ways(query)
            for placeholder, comment in comments.items():
                query = query.replace(placeholder, comment)
        # if fails, just return query
        except Exception as e:
            return query
        return query

    @staticmethod
    def add_node_or_ways(query):
        # if only has one node or way, duplicate line to include way or node
        if query.count("node") == 0:
            # get way line and duplicate and change to node
            # get node line and duplicate and change to way
            ways = re.search(r"(?P<space>\s+)?(?P<way>way(?P<query>.*))", query)
            query = query.replace(
                ways.group("way"),
                f"node{ways.group('query')}{ways.group('space')}{ways.group('way')}",
            )
        if query.count("way") == 0:
            # get node line and duplicate and change to way
            nodes = re.search(r"(?P<space>\s+)?(?P<node>node(?P<query>.*))", query)
            query = query.replace(
                nodes.group("node"),
                f"node{nodes.group('query')}{nodes.group('space')}{nodes.group('node')}",
            )
        return query

    @staticmethod
    def add_timeouts(query):
        if not re.search(r"[out:json][timeout:\d+]", query):
            query = query.replace("[out:json]", "[out:json][timeout:25]")
        return query


class OverpassAPIUtils:
    def __init__(self):
        self.api = overpy.Overpass()

    def query(self, query):
        return self.api.query(query)

    @staticmethod
    def create_feature():
        """Helper to create a QgsFeature"""
        fields = OverpassAPIUtils.feature_fields()
        return QgsFeature(fields)

    @staticmethod
    def feature_fields():
        fields = QgsFields()
        fields.append(QgsField("id", QVariant.Int))
        fields.append(QgsField("tags", QVariant.String))
        return fields

    @staticmethod
    def convert_resp_to_geom(query_resp: overpy.Result):
        """Convert overpy response to QGIS geometry"""
        # assume 'api' is an Overpy API object, and 'query' is an Overpy query object

        # loop over all nodes returned by the query
        point_features = OverpassAPIUtils.convert_nodes_to_points(query_resp)

        # loop over all ways returned by the query
        line_features, poly_lines = OverpassAPIUtils.convert_ways_to_lines(query_resp)

        poly_features = OverpassAPIUtils.convert_relations_to_areas(
            poly_lines, query_resp
        )
        # loop over all relations returned by the query
        # OverpassAPIUtils.convert_relations_to_areas(poly_features, query_resp)
        return point_features, line_features, poly_features

    @staticmethod
    def convert_relations_to_areas(poly_lines, query_resp):
        poly_features = []
        skip_ways = set()
        for relation in query_resp.relations:
            # create a list of Shapely LineString geometries from the relation members
            polygon = None
            for member in relation.members:
                if member.role == "outer":
                    member_way = member._result.get_way(member.ref)
                    # create a list of Shapely Point geometries from the member node coordinates
                    try:
                        points = [
                            QgsPoint(node.lon, node.lat) for node in member_way.nodes
                        ]
                    except DataIncomplete:
                        points = [
                            QgsPoint(node.lon, node.lat)
                            for node in member_way.get_nodes(resolve_missing=True)
                        ]
                    # create a Shapely Polygon geometry from the Point geometries
                    polygon = QgsPolygon(QgsLineString(points))
                    skip_ways.add(member.ref)
            # create a Shapely MultiLineString geometry from the LineString geometries
            # convert the Shapely geometry to a QGIS geometry

            qgs_geom = QgsGeometry.fromWkt(polygon.asWkt())
            # create a new QGIS feature with the geometry and any relevant relation attributes
            feature = OverpassAPIUtils.create_feature()
            feature.setGeometry(qgs_geom)
            tags = str(relation.tags)[:512] or ""

            feature.setAttributes([relation.id, tags])
            # add the feature to the list
            poly_features.append(feature)
        for way, polyline in poly_lines:
            # preference relations over polylines
            if way.id in skip_ways:
                continue
            polygon = QgsPolygon(polyline)
            qgs_geom = QgsGeometry.fromWkt(polygon.asWkt())
            # create a new QGIS feature with the geometry and any relevant relation attributes
            feature = OverpassAPIUtils.create_feature()
            feature.setGeometry(qgs_geom)
            tags = str(way.tags)[:512] or ""

            feature.setAttributes([way.id, tags])
            # add the feature to the list
            poly_features.append(feature)
        return poly_features

    @staticmethod
    def convert_nodes_to_points(query_resp):
        point_features = []
        for node in query_resp.nodes:
            # create a Shapely Point geometry from the node coordinates
            point = QgsPoint(node.lon, node.lat)
            # convert the Shapely geometry to a QGIS geometry
            qgs_geom = QgsGeometry.fromWkt(point.asWkt())
            # create a new QGIS feature with the geometry and any relevant node attributes
            feature = OverpassAPIUtils.create_feature()
            tags = str(node.tags)[:512] or ""
            feature.setGeometry(qgs_geom)
            feature.setAttributes([node.id, tags])
            # add the feature to the list
            point_features.append(feature)
        return point_features

    @staticmethod
    def convert_ways_to_lines(query_resp):
        line_features = []
        line_polys: list[tuple[overpy.Way, QgsLineString]] = []

        for way in query_resp.ways:
            # create a list of Shapely Point geometries from the way node coordinates
            try:
                points = [QgsPoint(node.lon, node.lat) for node in way.nodes]
            except DataIncomplete:
                points = [
                    QgsPoint(node.lon, node.lat)
                    for node in way.get_nodes(resolve_missing=True)
                ]
            # create a Shapely LineString geometry from the Point geometries
            linestring = QgsLineString(points)
            if linestring.isClosed():
                line_polys.append((way, linestring))
            # convert the Shapely geometry to a QGIS geometry
            qgs_geom = QgsGeometry.fromWkt(linestring.asWkt())
            # create a new QGIS feature with the geometry and any relevant way attributes
            feature = OverpassAPIUtils.create_feature()
            tags = str(way.tags)[:512] or ""
            feature.setGeometry(qgs_geom)
            feature.setAttributes([way.id, tags])
            # add the feature to the list
            line_features.append(feature)
        return line_features, line_polys

    @staticmethod
    def create_layers_from_resp(
        chat_gpt_resp: ChatGPTResponse, query_resp: overpy.Result
    ):
        """Create QGIS layers from overpy response"""
        layer_features = OverpassAPIUtils.convert_resp_to_geom(query_resp)
        # create a new QGIS vector layer with the features and appropriate geometry type

        point_layer = QgsVectorLayer(
            "Point?crs=EPSG:4326&field=id:double&field=tags:string(512,0)",
            f"{chat_gpt_resp.id} Points",
            "memory",
        )
        with edit(point_layer):
            point_layer.addFeatures(layer_features[0])

        line_layer = QgsVectorLayer(
            "LineString?crs=EPSG:4326&field=id:double&field=tags:string(512,0)",
            f"{chat_gpt_resp.id} Lines",
            "memory",
        )
        with edit(line_layer):
            line_layer.addFeatures(layer_features[1])

        poly_layer = QgsVectorLayer(
            "Polygon?crs=EPSG:4326&field=id:double&field=tags:string(512,0)",
            f"{chat_gpt_resp.id} Areas",
            "memory",
        )
        with edit(poly_layer):
            poly_layer.addFeatures(layer_features[2])

        return point_layer, line_layer, poly_layer
