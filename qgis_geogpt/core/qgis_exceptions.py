"""Exceptions that are tied to qgis exceptions such as QgsError so we can run unit tests.
Be careful here will stop the algorithm from running if raised"""


from qgis.core import QgsProcessingException
