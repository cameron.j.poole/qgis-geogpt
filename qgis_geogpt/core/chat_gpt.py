"""This class encapsulates our chat gpt API calls.
Possible to replace with  openai library but prefer not to have external dependencies
import openai

# Load your API key from an environment variable or secret management service
openai.api_key = os.getenv("OPENAI_API_KEY")

response = openai.Completion.create(model="text-davinci-003", prompt="Say this is a test", temperature=0, max_tokens=7)
"""
import enum
import logging
import os
import re
from dataclasses import dataclass

import requests  # can't remember if requests comes built into PyQGIS

from qgis_geogpt.core.exceptions import FailedToFetchMessageError

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


@dataclass
class ChatGPTResponse:
    id: str
    object: str
    created: int
    choices: list
    usage: dict
    model: str = None


CHATGPT_COMPLETIONS_API_URL = (
    "https://api.openai.com/v1/completions"  # use with text-davinci-003
)
CHATGPT_API_URL = "https://api.openai.com/v1/chat/completions"


class QueryType(enum.Enum):
    overpass: str = "overpass"
    wikipedia: str = "wikipedia"


class ChatGPTUtils:
    """
    This class encapsulates our chat gpt API calls.

    """

    def __init__(
        self,
        token_limit=2048,
        query_type=QueryType.overpass,
        api_key=None,
        api_model="gpt-3.5-turbo",
        log=logger,
    ):
        """Initialize the class."""
        self.api_key = api_key or os.environ.get("CHAT_GPT_API_KEY")
        self.api_url = CHATGPT_API_URL
        self.api_model = api_model or os.environ.get("CHAT_GPT_API_MODEL")
        self.token_limit = token_limit
        self.query_type = query_type
        self.log = log
        self.messages = []

    def add_query(self, query: str):
        self.messages.append({"role": "user", "content": query})

    def query_api(self, text: str) -> ChatGPTResponse:
        """Query the API with the given text and return the response."""
        # set the request headers
        # convert query to a format that the API expects
        self.add_query(self.wrap_query(text))
        if not self.api_key:
            raise ValueError("API key not set")
        if not self.api_model:
            raise ValueError("API model not set")
        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {self.api_key}",
        }

        # set the request body

        data = {
            "messages": self.messages,
            "max_tokens": self.token_limit,
            "model": self.api_model,
        }

        try:
            # send the API request and receive the response
            response = requests.post(self.api_url, headers=headers, json=data)
            response.raise_for_status()
            # Report 401 Client Error: Unauthorized for url: https://api.openai.com/v1/chat/completions
            # Report 400 Bad Request: Invalid request body for url: https://api.openai.com/v1/chat/completions
            # json has message and type
        except requests.exceptions.RequestException as e:
            self.log_message(message=f"Failed to fetch message from API: {e}", level=1)
            raise FailedToFetchMessageError(e)

        # extract the response data
        return ChatGPTResponse(**response.json())

    def extract_code(self, response: ChatGPTResponse) -> list:
        """Extract any code from the API response."""
        pattern = r"```(.*?)```"
        content = response.choices[0].get("message").get("content")

        # apply the pattern to the response and extract all matches
        matches = re.findall(pattern, content, flags=re.DOTALL)

        # might have returned exact query check for that
        # all may not have backticks in example
        # \n\n[out:json];\narea[name="Hobart"]->.searchArea;\nway(area.searchArea);\nout body;\n>;\nout skel qt; \n\n
        if not matches and self.query_type == QueryType.overpass:
            if "[out:json]" in content:
                matches = [content]
        return matches if matches else []

    def wrap_query(self, query: str) -> str:
        """Wrap the query in the appropriate text.
        We may need to find prefix and suffix text for each query type."""
        if self.query_type == QueryType.overpass:
            return (
                f"Create an Overpass API query example to get the following using `out body` "
                f"to be able to convert to geometries : {query}"
            )
        if self.query_type == QueryType.wikipedia:
            return f"Create a sparql wikidata query including location for the following: {query}"

        return f"Query: {query}\nResponse:"

    def log_message(self, message: str, level=logging.DEBUG):
        self.log.log(msg=message, level=level)
