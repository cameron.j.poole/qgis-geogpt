"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingContext,
    QgsProcessingFeedback,
    QgsProcessingParameterNumber,
    QgsProcessingParameterString,
    QgsProject,
)
from qgis.PyQt.QtCore import QCoreApplication

from qgis_geogpt.core.exceptions import FailedTogetOverpassDataError, NoQueryExtracted
from qgis_geogpt.core.overpass_api import OverpassAPIUtils, OverpassQueryUtils
from qgis_geogpt.core.qgis_chat_gpt_util import QgsChatGPTUtils
from qgis_geogpt.toolbelt.debugger import RemoteDebugger
from qgis_geogpt.utils.overpy.exception import OverpassBadRequest


class ChatGPTOverPassAPIProcessingAlgorithm(QgsProcessingAlgorithm):
    """Processing algorithm for Chat GPT query Overpass API"""

    QUERY_PROMPT = "query_prompt"
    ATTEMPTS = "attempts"

    def __init__(self):
        super().__init__()
        self.rdb = None
        self.chat_gpt = None
        self.overpy_util = None

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate("Processing", string)

    def createInstance(self):
        return ChatGPTOverPassAPIProcessingAlgorithm()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "chat_gpt_query_overpass"

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr("Chat GPT query Overpasss")

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr("ChatGPT")

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, an d must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return "chatgpt_overpass"

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr("")

    def initAlgorithm(self, config=None):
        """Initialise algorithm."""

        self.addParameter(
            QgsProcessingParameterString(
                self.QUERY_PROMPT,
                self.tr("Query to feed Chat GPT"),
                multiLine=True,
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.ATTEMPTS,
                self.tr("Number of attempts to chatGPT"),
                defaultValue=2,
            )
        )

    def processAlgorithm(
        self,
        parameters,
        context: QgsProcessingContext,
        model_feedback: QgsProcessingFeedback,
    ):
        """
        Here is where the processing itself takes place.
        """

        # Retrieve the feature source and sink. The 'dest_id' variable is used
        # to uniquely identify the feature sink, and must be included in the
        # dictionary returned by the processAlgorithm function.

        alg_output = {}
        query_prompt = self.parameterAsString(parameters, self.QUERY_PROMPT, context)
        model_feedback.setProgressText(self.tr("Querying Chat GPT"))
        self.chat_gpt = QgsChatGPTUtils()
        self.overpy_util = OverpassAPIUtils()
        no_attempts = self.parameterAsInt(parameters, self.ATTEMPTS, context)
        overpy_resp = None
        chat_gpt_resp = None
        # while no results found and less than attempts try again
        self.rdb = RemoteDebugger()

        i = 0
        for i in range(no_attempts):
            try:
                (
                    chat_gpt_resp,
                    improved_query,
                    overpass_query,
                ) = self.fetch_chat_gpt_data(query_prompt)
                self.rdb.start_debugging()
                overpy_resp = self.fetch_overpass_data(
                    improved_query, model_feedback, overpass_query
                )
                break
            except NoQueryExtracted:
                continue
            except FailedTogetOverpassDataError:
                continue
            except OverpassBadRequest:
                continue
        if (i == no_attempts - 1) and overpy_resp is None:
            model_feedback.reportError(
                self.tr("Failed to get Overpass data. Aborting."), True
            )
            raise FailedTogetOverpassDataError
        layers = OverpassAPIUtils.create_layers_from_resp(chat_gpt_resp, overpy_resp)
        for vl in layers:
            QgsProject.instance().addMapLayer(vl)
            assert vl.isValid()
        alg_output["point_layer"] = layers[0].id()
        alg_output["line_layer"] = layers[1].id()
        alg_output["polygon_layer"] = layers[2].id()
        return alg_output

    def fetch_chat_gpt_data(self, query_prompt):
        resp = self.chat_gpt.query_api(query_prompt)
        extracted_code = self.chat_gpt.extract_code(resp)
        if len(extracted_code) == 1:
            overpass_query = extracted_code[0]
        elif len(extracted_code) > 1:
            # TODO: handle multiple queries in response
            overpass_query = extracted_code[0]
        else:
            raise NoQueryExtracted
        improved_query = OverpassQueryUtils.improve_query(overpass_query)
        # write response to window
        self.chat_gpt.write_query_to_layer(resp, overpass_query, improved_query)
        self.rdb.start_debugging()
        return resp, improved_query, overpass_query

    def fetch_overpass_data(self, improved_query, model_feedback, overpass_query):
        if not overpass_query:
            model_feedback.pushWarning(
                self.tr("No query found in Chat GPT response. Aborting.")
            )
            raise NoQueryExtracted
        model_feedback.setProgressText(self.tr("Querying Overpass API"))
        self.rdb.start_debugging()
        overpy_resp = self.overpy_util.query(overpass_query)
        if not overpy_resp.nodes and not overpy_resp.ways and not overpy_resp.relations:
            model_feedback.pushWarning(
                self.tr("No results found for query, attempting to repair")
            )
            overpy_resp = self.overpy_util.query(improved_query)
        if not overpy_resp.nodes and not overpy_resp.ways and not overpy_resp.relations:
            model_feedback.reportError(
                "No results found for repaired query or original", True
            )
            raise FailedTogetOverpassDataError
        return overpy_resp
