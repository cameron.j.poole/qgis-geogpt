import ast
import os
import pathlib
import sys

from qgis_geogpt.toolbelt import PlgLogger

current_dir = pathlib.Path(__file__).parent.resolve()
venv_folder_default = current_dir.parent.parent.resolve() / ".venv"


class RemoteDebugger:
    """A remote debugger for debugging QGIS plugins with Pycharm remote debugger
    Add this to your plugin's __init__.py file:
    """

    def __init__(self):
        self.log = PlgLogger()
        if os.environ.get("DEBUG", False):
            self.log.log("Starting remote debugger")
            self.port = os.environ.get("DEBUG_PORT", 5678)
            self._start_remote_debugger()
        else:
            self.log.log("Not starting remote debugger")

    def _start_remote_debugger(self):
        if sys.platform == "win32":
            sys.path.append(
                os.environ.get("PYCHARM_DEBUG", venv_folder_default / "Scripts")
            )
            sys.path.append(
                os.environ.get(
                    "PYCHARM_DEBUG", venv_folder_default / "Lib" / "site-packages"
                )
            )
        else:
            sys.path.append(
                os.environ.get("PYCHARM_DEBUG", venv_folder_default / "bin")
            )
            sys.path.append(
                os.environ.get(
                    "PYCHARM_DEBUG",
                    venv_folder_default / "lib" / "python3.10" / "site-packages",
                )
            )

    def start_debugging(self):
        # get debug from envars and convert to bool
        if ast.literal_eval(os.environ.get("DEBUG", "False")):
            import pydevd_pycharm

            pydevd_pycharm.settrace(
                "localhost", port=self.port, stdoutToServer=True, stderrToServer=True
            )
