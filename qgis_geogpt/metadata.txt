[general]
name=GeoGPT
about=Uses ChatGPT API to convert text to Overpass API (OSM) and Wikidata Queries
category=Web
hasProcessingProvider=yes
description=Uses ChatGPT API to convert text to Overpass API (OSM) and Wikidata Queries
icon=resources/images/default_icon.png
tags=chatgpt,osm,overpassapi,wikidata

# credits and contact
author=Cameron POOLE
email=Cameron@mammothgeospatial.com
homepage=https://gitlab.com/mammoth-geospatial/qgis_geogpt
repository=https://gitlab.com/mammoth-geospatial/qgis_geogpt
tracker=https://gitlab.com/mammoth-geospatial/qgis_geogpt/-/issues

# experimental flag
deprecated=False
experimental=True
qgisMinimumVersion=3.28
qgisMaximumVersion=3.99

# versioning
version=0.1.0
changelog=
