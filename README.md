# QGIS GeoGPT - QGIS Plugin

__Fetch OSM Data using ChatGPT__

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)

## A QGIS Plugin for Generating Data using ChatGPT, Overpass API, and Open Street Map Data

This QGIS plugin allows users to generate data using a combination of ChatGPT, Overpass API, and Open Street Map data.
With this plugin, users can easily generate large amounts of data for their GIS projects without having to manually
write overpass API queries.

## Background

ChatGPT is a large language model trained by OpenAI, which is capable of generating human-like text based on a given
prompt.
The Overpass API is a tool that allows users to extract data from OpenStreetMap, which is a free, open-source map of the
world.
By combining these tools, users can generate Overpass queries using ChatGPT, and then use the Overpass API to extract
data from OpenStreetMap.

**Note:** This plugin is still in development and is not yet ready for production use.

## Installation

You will need a ChatGPT API key to use this plugin. You can get one by signing up for a free account at
[OpenAI](https://platform.openai.com/).

### Install Zip File

### Step 1 - Download the zip file

Go to releases page and download the latest release.
In Qgis go to `Plugins`-> `Manage And Install Plugins` -> `Install from ZIP`
Select the zip file you downloaded and click `Install Plugin`

### Step 2 - Add your API key

Add your api key to the Plugin Settings in QGIS.
1. Go to `Plugins`-> `Manage And Install Plugins` -> `Settings`
2. Select `ChatGPT` from the list of plugins
3. Add your API key to the `API Key` field
4. Click `OK`
5. Click `Close`


[//]: # (To install the plugin, follow these steps:)
[//]: # (    Open QGIS and navigate to the Plugin Manager &#40;Plugins > Manage and Install Plugins&#41;)
[//]: # (    Click on the "Settings" tab and ensure that the "Show also experimental plugins" option is checked.)
[//]: # (    Search for "ChatGPT" in the search bar and click "Install" to install the plugin.)
[//]: # (    Once the plugin is installed, it will appear in the "Installed" tab of the Plugin Manager.)
[//]: # (    Click on the checkbox next to the plugin name to activate it.)

Once the plugin is installed and activated, users can begin using it to generate data for their GIS projects.

### Tooling

This project is configured with the following tools:

- [Black](https://black.readthedocs.io/en/stable/) to format the code without any existential question
- [iSort](https://pycqa.github.io/isort/) to sort the Python imports

Code rules are enforced with [pre-commit](https://pre-commit.com/) hooks.  
Static code analisis is based on: Flake8

See also: [contribution guidelines](CONTRIBUTING.md).

### Documentation

The documentation is generated using Sphinx and is automatically generated through the CI and published on Pages.

- homepage: <https://gitlab.com/mammoth-geospatial/qgis_geogpt>
- repository: <https://gitlab.com/mammoth-geospatial/qgis_geogpt>
- tracker: <https://gitlab.com/mammoth-geospatial/qgis_geogpt/-/issues>

----

## License

Distributed under the terms of the [`GPLv3` license](LICENSE).
OverPy is licensed under the terms of the [MIT license](https://github.com/DinoTools/python-overpy/blob/master/LICENSE)

## Credits

Python Overpy - for converting Overpass API queries to OSM data. I've modified the code to work with QGIS.

## Related Projects

I actually built this before I knew about https://github.com/Mariosmsk/QChatGPT/blob/main/qchatqpt.py.
I reused the install_dependencies from this project. TODO
