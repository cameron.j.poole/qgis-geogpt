# Testing dependencies
# --------------------

pytest-cov>=3,<4
packaging>=23
pytest-qgis
pytest
pytest-mock
requests-mock==1.10.0
requests
