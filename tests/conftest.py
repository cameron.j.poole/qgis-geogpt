from pathlib import Path

import pytest


@pytest.fixture(scope="session")
def testing_dir():
    return Path(__file__).parent.resolve()


@pytest.fixture(scope="session")
def data_dir(testing_dir):
    return testing_dir / "data"
