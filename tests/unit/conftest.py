import pytest

from qgis_geogpt.core.chat_gpt import ChatGPTUtils


@pytest.fixture(scope="session")
def chat_gpt():
    return ChatGPTUtils()
