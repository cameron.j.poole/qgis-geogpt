from qgis_geogpt.core.chat_gpt import ChatGPTResponse


def test_query_api(requests_mock, chat_gpt):
    requests_mock.post(
        chat_gpt.api_url,
        json={
            "id": "chatcmpl-123",
            "object": "chat.completion",
            "created": 1677652288,
            "choices": [
                {
                    "index": 0,
                    "message": {
                        "role": "assistant",
                        "content": "\n\nHello there, how may I assist you today?",
                    },
                    "finish_reason": "stop",
                }
            ],
            "usage": {"prompt_tokens": 9, "completion_tokens": 12, "total_tokens": 21},
        },
    )
    resp = chat_gpt.query_api("Find all the parks in Hobart")
    assert requests_mock.called_once
    assert isinstance(resp, ChatGPTResponse)


def test_extract_code():
    ChatGPTResponse(
        id="chatcmpl-7BBZ4laJxJ8DDEMHw3e55clFKjTUP",
        object="chat.completion",
        created=1682900418,
        choices=[
            {
                "message": {
                    "role": "assistant",
                    "content": '[out:json];\narea[name="Hobart"]->.searchArea;\nway[highway](area.searchArea);\nout body;\n>;\nout skel qt;',
                },
                "finish_reason": "stop",
                "index": 0,
            }
        ],
        usage={"prompt_tokens": 27, "completion_tokens": 33, "total_tokens": 60},
        model="gpt-3.5-turbo-0301",
    )
