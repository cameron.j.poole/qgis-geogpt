from qgis.core import QgsProcessingFeedback, QgsProject, QgsVectorLayer


class ConsoleFeedBack(QgsProcessingFeedback):
    _errors = []
    _info = []

    def reportError(self, error, fatalError=False):
        print(error)
        self._errors.append(error)

    def pushDebugInfo(self, info):
        print(info)
        self._info.append(info)


def get_clean_layer(output_files, layer_type, layer_name):
    vl = QgsVectorLayer(output_files[layer_type], layer_name, "ogr")
    QgsProject.instance().addMapLayer(vl)
    if vl.isValid():
        return vl
