import pickle

import pytest
from qgis.core import QgsProject

from qgis_geogpt.core.chat_gpt import ChatGPTResponse
from qgis_geogpt.core.qgis_chat_gpt_util import QgsChatGPTUtils


@pytest.fixture(scope="session")
def overpass_result(data_dir):
    """This contains all roads in hobart"""
    return pickle.load(open(data_dir / "overpy_area_test.pkl", "rb"))


@pytest.fixture(scope="session")
def chat_gpt_response():
    return ChatGPTResponse(
        id="chatcmpl-7BEjenFBHjGNVfAihGbsHSqBY9bES",
        object="chat.completion",
        created=1682912606,
        choices=[
            {
                "message": {
                    "role": "assistant",
                    "content": 'The following Overpass API query can be used to find all the parks in Hobart, Tasmania:\n\n```\n[out:json];\narea["name"="Hobart"]->.searchArea;\nway(area.searchArea)["leisure"="park"];\nout center;\n```\n\nThis query starts by defining the search area as the city of Hobart, Tasmania. Then it searches for all the ways (roads, paths, etc.) within this area that have the tag "leisure"="park". Finally, it outputs the center point of each park found in the search area in JSON format.',
                },
                "finish_reason": "stop",
                "index": 0,
            }
        ],
        usage={"prompt_tokens": 77, "completion_tokens": 119, "total_tokens": 196},
        model="gpt-3.5-turbo-0301",
    )


@pytest.fixture()
def test_project(qgis_new_project):
    return QgsProject.instance()


@pytest.fixture()
def chat_gpt_qgis():
    return QgsChatGPTUtils(api_key="Fake API Key")
