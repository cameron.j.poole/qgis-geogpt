"""Test the processing algorithm.

TODO - add mocks for the overpass query and the chat gpt query so we don't wait testing tokens
"""
import pytest_qgis  # noqa
from qgis import processing
from qgis.core import QgsProcessingContext, QgsProcessingUtils  # noqa

from qgis_geogpt.processing.chatgpt_overpass import (
    ChatGPTOverPassAPIProcessingAlgorithm,
)

from .utils import ConsoleFeedBack


def test_run_processing(
    mocker,
    chat_gpt_response,
    overpass_result,
    qgis_processing,
    testing_dir,
    test_project,
):
    mocker.patch(
        "qgis_geogpt.core.qgis_chat_gpt_util.QgsChatGPTUtils.query_api",
        return_value=chat_gpt_response,
    )
    mocker.patch(
        "qgis_geogpt.core.overpass_api.OverpassAPIUtils.query",
        return_value=overpass_result,
    )

    context = QgsProcessingContext()
    context.setProject(test_project)
    feedback = ConsoleFeedBack()
    alg = ChatGPTOverPassAPIProcessingAlgorithm()
    output = processing.run(
        alg,
        {
            "QUERY_PROMPT": "Find all the parks in Hobart, Tasmania",
        },
        context=context,
        feedback=feedback,
    )
    testing_layer_points = QgsProcessingUtils.mapLayerFromString(
        output["point_layer"], context, True
    )
    testing_layer_lines = QgsProcessingUtils.mapLayerFromString(
        output["line_layer"], context, True
    )
    testing_layer_polygons = QgsProcessingUtils.mapLayerFromString(
        output["polygon_layer"], context, True
    )

    assert len(feedback._info) == 0
    assert len(feedback._errors) == 0
    assert testing_layer_polygons.isValid()
    assert testing_layer_points.isValid()
    assert testing_layer_lines.isValid()
    testing_layer_points.dataProvider().reloadData()
    testing_layer_polygons.dataProvider().reloadData()
    testing_layer_lines.dataProvider().reloadData()
    assert testing_layer_polygons.featureCount() > 0
    assert testing_layer_points.featureCount() > 0
    assert testing_layer_lines.featureCount() > 0
