#! python3  # noqa E265

"""
    Usage from the repo root folder:

    .. code-block:: bash

        # for whole tests
        python -m unittest tests.qgis.test_plg_preferences
        # for specific test
        python -m unittest tests.qgis.test_plg_preferences.TestPlgPreferences.test_plg_preferences_structure
"""

# standard library
from qgis.testing import unittest

# project
from qgis_geogpt.__about__ import __version__
from qgis_geogpt.toolbelt.preferences import PlgOptionsManager, PlgSettingsStructure

# ############################################################################
# ########## Classes #############
# ################################


class TestPlgPreferences(unittest.TestCase):
    def test_plg_preferences_structure(self):
        """Test settings types and default values."""
        settings = PlgSettingsStructure()

        # global
        self.assertTrue(hasattr(settings, "debug_mode"))
        self.assertIsInstance(settings.debug_mode, bool)
        self.assertEqual(settings.debug_mode, False)

        self.assertTrue(hasattr(settings, "version"))
        self.assertIsInstance(settings.version, str)
        self.assertEqual(settings.version, __version__)

        self.assertTrue(hasattr(settings, "chat_gpt_api_key"))
        self.assertIsInstance(settings.chat_gpt_api_key, str)
        self.assertEqual(settings.chat_gpt_api_key, "")

        self.assertTrue(hasattr(settings, "chat_gpt_api_model"))
        self.assertIsInstance(settings.chat_gpt_api_model, str)
        self.assertEqual(settings.chat_gpt_api_model, "")


# ############################################################################
# ####### Stand-alone run ########
# ################################
if __name__ == "__main__":
    unittest.main()
