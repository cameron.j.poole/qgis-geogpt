from qgis.core import edit

from qgis_geogpt.core.qgis_chat_gpt_util import QgsChatGPTUtils


def test_q_chat_gpt_fields():
    assert len(QgsChatGPTUtils.layer_fields()) > 0


def test_q_chat_gpt_layer(chat_gpt_qgis, test_project):
    assert QgsChatGPTUtils().get_qgis_layer() is not None
    assert chat_gpt_qgis.helper_layer_name == "chat_gpt_responses"


def test_get_qgis_layer(chat_gpt_qgis):
    """Test the conversion of a chat gpt response to a qgis layer."""
    assert chat_gpt_qgis.get_qgis_layer() is not None


def test_create_query_feet(chat_gpt_qgis, chat_gpt_response):
    """Test the conversion of a chat gpt response to a qgis layer."""
    feature = chat_gpt_qgis.create_query_feet()
    QgsChatGPTUtils.set_feature_attributes(
        feature=feature,
        response=chat_gpt_response,
        overpass_query="overpass_query",
        improved_overpass_query="improved_overpass_query",
    )
    assert feature is not None
    assert feature.attributeMap().get("id") == chat_gpt_response.id
    assert feature.attributeMap().get("model") == chat_gpt_response.model

    with edit(chat_gpt_qgis.layer):
        chat_gpt_qgis.layer.addFeature(feature)
    assert chat_gpt_qgis.layer.featureCount() == 1


def test_chat_gpt_query(requests_mock, chat_gpt_qgis, chat_gpt_response):
    """Test the conversion of a chat gpt response to a qgis layer."""
    requests_mock.post(
        chat_gpt_qgis.api_url,
        json=chat_gpt_response.__dict__,
        status_code=200,
    )
    assert (
        chat_gpt_qgis.query_api(text="Find all the parks in Hobart")
        == chat_gpt_response
    )
