from qgis_geogpt.core.overpass_api import OverpassAPIUtils, OverpassQueryUtils


def test_convert_resp_to_geom(overpass_result):
    geometries = OverpassAPIUtils.convert_resp_to_geom(overpass_result)
    assert geometries is not None
    assert len(geometries) == 3
    assert geometries[0] is not None


def test_convert_resp_to_qgis_layers(overpass_result, chat_gpt_response):
    layers = OverpassAPIUtils.create_layers_from_resp(
        chat_gpt_response, overpass_result
    )
    assert layers is not None
    assert len(layers) == 3
    assert layers[0].name() == f"{chat_gpt_response.id} Points"
    assert layers[1].name() == f"{chat_gpt_response.id} Lines"
    assert layers[2].name() == f"{chat_gpt_response.id} Areas"
    assert layers[0].featureCount() == 7420
    assert layers[1].featureCount() == 454
    assert layers[2].featureCount() == 445


def test_improve_query():
    weak_query = """
[out:json][timeout:25];
area[name="Bristol"]->.searchArea;
way(area.searchArea)["name"="Bristol Street"];
out center;
    """
    improved_query = """
[out:json][timeout:25];
area[name="Bristol"]->.searchArea;
node(area.searchArea)["name"="Bristol Street"];
way(area.searchArea)["name"="Bristol Street"];
(._;>;);
out body;
(._;>;);
out skel qt;
    """
    assert improved_query == OverpassQueryUtils.improve_query(weak_query)
    query = """
[out:json][timeout:25];
// gather results
(
  // query buildings in North Hobart
  way["building"](around:1000,-42.8672,147.3183);
);
// make the output as a body so it can be converted to geometries
out body;
"""
    improved_query = """
[out:json][timeout:25];
// gather results
(
  // query buildings in North Hobart
  node["building"](around:1000,-42.8672,147.3183);
  way["building"](around:1000,-42.8672,147.3183);
);
// make the output as a body so it can be converted to geometries
(._;>;);
out body;
"""
    assert improved_query == OverpassQueryUtils.improve_query(query)


def test_replace_comments():
    query = """
    [out:json][timeout:25];
    // gather results
    (
      // query buildings in North Hobart
      way["building"](around:1000,-42.8672,147.3183);
    );
    // make the output as a body so it can be converted to geometries
    out body;
    """
    clean_query, comments = OverpassQueryUtils.replace_comments(query)
    assert clean_query is not None
    assert len(comments) == 3


def test_recurse_query():
    query = """
    [out:json][timeout:25];
    (
      way["building"](around:1000,-42.8672,147.3183);
    );
    out body;
    out skel qt;
    """
    clean_query = OverpassQueryUtils.fix_missing_recurse(query)
    assert clean_query is not None
